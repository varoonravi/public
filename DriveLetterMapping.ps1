﻿$diskDrive = Get-WmiObject -Class "Win32_DiskDrive" -ComputerName "."
$logicalDiskToPartition = Get-WmiObject -Class "Win32_LogicalDiskToPartition" -ComputerName "."
$driveLetterDict = @{}
$deviceNameDict = @{}
$path = "\\NUCLEUS\Users\Administrator.ABC\Desktop\Shared\sql.txt"
$deviceNameDict.Add("Bus Number 0, Target ID 78, LUN 0","xvdca")
$deviceNameDict.Add("Bus Number 0, Target ID 79, LUN 0","xvdcb")
$deviceNameDict.Add("Bus Number 0, Target ID 80, LUN 0","xvdcc")
$deviceNameDict.Add("Bus Number 0, Target ID 81, LUN 0","xvdcd")
$deviceNameDict.Add("Bus Number 0, Target ID 82, LUN 0","xvdce")
$deviceNameDict.Add("Bus Number 0, Target ID 83, LUN 0","xvdcf")
$deviceNameDict.Add("Bus Number 0, Target ID 84, LUN 0","xvdcg")
$deviceNameDict.Add("Bus Number 0, Target ID 85, LUN 0","xvdch")
$deviceNameDict.Add("Bus Number 0, Target ID 86, LUN 0","xvdci")
$deviceNameDict.Add("Bus Number 0, Target ID 87, LUN 0","xvdcj")
$deviceNameDict.Add("Bus Number 0, Target ID 88, LUN 0","xvdck")
$deviceNameDict.Add("Bus Number 0, Target ID 89, LUN 0","xvdcl")
$deviceNameDict.Add("Bus Number 0, Target ID 0, LUN 0","/dev/sda1")
$deviceNameDict.Add("Bus Number 0, Target ID 1, LUN 0","xvdb")
$deviceNameDict.Add("Bus Number 0, Target ID 2, LUN 0","xvdc")
$deviceNameDict.Add("Bus Number 0, Target ID 3, LUN 0","xvdd")
$deviceNameDict.Add("Bus Number 0, Target ID 4, LUN 0","xvde")
$deviceNameDict.Add("Bus Number 0, Target ID 5, LUN 0","xvdf")
$deviceNameDict.Add("Bus Number 0, Target ID 6, LUN 0","xvdg")
$deviceNameDict.Add("Bus Number 0, Target ID 7, LUN 0","xvdh")
$deviceNameDict.Add("Bus Number 0, Target ID 8, LUN 0","xvdi")
$deviceNameDict.Add("Bus Number 0, Target ID 9, LUN 0","xvdj")
$deviceNameDict.Add("Bus Number 0, Target ID 10, LUN 0","xvdk")
$deviceNameDict.Add("Bus Number 0, Target ID 11, LUN 0","xvdl")
$deviceNameDict.Add("Bus Number 0, Target ID 12, LUN 0","xvdm")
$deviceNameDict.Add("Bus Number 0, Target ID 13, LUN 0","xvdn")
$deviceNameDict.Add("Bus Number 0, Target ID 14, LUN 0","xvdo")
$deviceNameDict.Add("Bus Number 0, Target ID 15, LUN 0","xvdp")
$deviceNameDict.Add("Bus Number 0, Target ID 16, LUN 0","xvdq")
$deviceNameDict.Add("Bus Number 0, Target ID 17, LUN 0","xvdr")
$deviceNameDict.Add("Bus Number 0, Target ID 18, LUN 0","xvds")
$deviceNameDict.Add("Bus Number 0, Target ID 19, LUN 0","xvdt")
$deviceNameDict.Add("Bus Number 0, Target ID 20, LUN 0","xvdu")
$deviceNameDict.Add("Bus Number 0, Target ID 21, LUN 0","xvdv")
$deviceNameDict.Add("Bus Number 0, Target ID 22, LUN 0","xvdw")
$deviceNameDict.Add("Bus Number 0, Target ID 23, LUN 0","xvdx")
$deviceNameDict.Add("Bus Number 0, Target ID 24, LUN 0","xvdy")
$deviceNameDict.Add("Bus Number 0, Target ID 25, LUN 0","xvdz")

foreach($i in $logicalDiskToPartition)
{
    $regex1 = "DeviceID"
    $i.antecedent -match "#\d"
    $index = $Matches[0].Substring(1,1)
    $i.dependent -match "=\W\w"
    $driveLetter = $Matches[0].Substring(2,1)
    $driveLetterDict.Add($index,$driveLetter)

}

if(Test-Path $path)
{
    Remove-Item $path
}

foreach($i in $diskDrive)
{
   $metaStr = "Bus Number "+$i.SCSIBus+", Target ID "+$i.SCSITargetId+", LUN "+$i.SCSILogicalUnit
   $str = $driveLetterDict.Get_Item($i.Index.ToString())+"-"+$deviceNameDict[$metaStr]
   $str
   #$str | Out-File -FilePath $path -Append
   Add-Content $path $str
   
   
}

